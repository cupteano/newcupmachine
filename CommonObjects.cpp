
#include "CommonObjects.h"

namespace Cupteano {
    
    constexpr int safeDegrees(int value, int min, int max)
    {
        return
        value >= min && value < max ? value :
        value <  min ? safeDegrees(value + 360, min, max) :
        value >= max ? safeDegrees(value - 360, min, max) :
        value;
    }

    int recToRobotCoordinate(int recDegree)
    {
            int robotDegree = (recDegree - 90) * -1;
            if (robotDegree > 180) {
                robotDegree -= 360;
            } else if (robotDegree <= -180) {
                robotDegree += 360;
            }
            return robotDegree;
    }

    int robotToRecCoordinate(int robotDegree)
    {
        int recDegree = safeDegrees((robotDegree * -1) + 90, 0, 360);
        return recDegree;
    }
    
}
