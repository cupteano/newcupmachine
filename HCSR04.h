#ifndef HCSR04_h
#define HCSR04_h

#include "arduino.h"

namespace Cupteano {

    class HCSR04 {
    
    private:
        static constexpr int sonicSpeed = 340; // meter per seconds
    
        const int trigPin;
        const int echoPin;

    public:
        HCSR04(uint8_t _trigPin, uint8_t _echoPin);
        int getRawValue();
        float getDistanceAsCentimeter();
        operator float();
         
    };

}

#endif
