
#ifndef AppDelegate_h
#define AppDelegate_h

#include "arduino.h"

#include "Switches.h"
#include "IRSerial.h"
#include "HMC5883L.h"
#include "MotorDriver.h"
#include "CommonObjects.h"
#include "MovingAverage.h"
#include "Kicker.h"
#include "LineSensor.h"
#include "HoldSensor.h"
#include "HCSR04.h"

class AppDelegate {

private:
    Cupteano::Switches toggleSwitch = Cupteano::Switches(2);
    Cupteano::IRSerial ballSensor = Cupteano::IRSerial(&Serial2);

    Cupteano::HMC5883L compass = Cupteano::HMC5883L(Wire);

    Cupteano::MotorDriver motorLeft  = Cupteano::MotorDriver(23, 22, 7);
    Cupteano::MotorDriver motorBack  = Cupteano::MotorDriver(27, 26, 3);
    Cupteano::MotorDriver motorRight = Cupteano::MotorDriver(25, 24, 5);

    Cupteano::MovingAverage ballDistanceSmoother = Cupteano::MovingAverage(20);
    Cupteano::MovingAverage ballAngleSmoother = Cupteano::MovingAverage(20);

    Cupteano::Kicker kicker = Cupteano::Kicker(31, 30);

    Cupteano::HoldSensor holdSensor = Cupteano::HoldSensor(A9);

    Cupteano::HCSR04 sonicLeft  = Cupteano::HCSR04(46, 44);
    Cupteano::HCSR04 sonicBack  = Cupteano::HCSR04(42, 40);
    Cupteano::HCSR04 sonicRight = Cupteano::HCSR04(38, 36);

    Cupteano::MovingAverage compassSmoother = Cupteano::MovingAverage(20);

    Cupteano::MovingAverage sonicLSmoother = Cupteano::MovingAverage(20);
    Cupteano::MovingAverage sonicBSmoother = Cupteano::MovingAverage(20);
    Cupteano::MovingAverage sonicRSmoother = Cupteano::MovingAverage(20);

    /* Robot type */
    #define RED
//    #define GREEN
    /* Field type */
    #define A
//    #define B
    

    #if defined(RED) && defined(A)

    Cupteano::LineSensor lineLeft  = Cupteano::LineSensor(A3, 500);
    Cupteano::LineSensor lineRight = Cupteano::LineSensor(A7, 500);
    Cupteano::LineSensor lineFront = Cupteano::LineSensor(A5, 500);
    Cupteano::LineSensor lineBack  = Cupteano::LineSensor(A1, 500);

    #elif defined(GREEN) && defined(A)
    

    Cupteano::LineSensor lineLeft  = Cupteano::LineSensor(A3, 700);
    Cupteano::LineSensor lineRight = Cupteano::LineSensor(A5, 700);
    Cupteano::LineSensor lineFront = Cupteano::LineSensor(A7, 700);
    Cupteano::LineSensor lineBack  = Cupteano::LineSensor(A1, 700);

    #elif defined(RED) && defined(B)

    Cupteano::LineSensor lineLeft  = Cupteano::LineSensor(A3, 600);
    Cupteano::LineSensor lineRight = Cupteano::LineSensor(A5, 600);
    Cupteano::LineSensor lineFront = Cupteano::LineSensor(A7, 300);
    Cupteano::LineSensor lineBack  = Cupteano::LineSensor(A1, 300);

    #elif defined(GREEN) && defined(B)

    Cupteano::LineSensor lineLeft  = Cupteano::LineSensor(A3, 600);
    Cupteano::LineSensor lineRight = Cupteano::LineSensor(A5, 600);
    Cupteano::LineSensor lineFront = Cupteano::LineSensor(A7, 300);
    Cupteano::LineSensor lineBack  = Cupteano::LineSensor(A1, 300);

    #endif

    int x, y;

    int angle = 0;

    static constexpr int goalX = 0;
    static constexpr int goalY = 100;

    static constexpr uint8_t tonePin = 11;
    
    void mainLoop();
    void calibration();
    void run(int, int);
    int turn(int);
    int ballTrack();
    void getPosition();
    void kicking();

    void move(int degree, int speed);

    int PIDController(int);

public:
    AppDelegate();
    void begin();
    
};

#endif
