#ifndef HMC5883L_h
#define HMC5883L_h

#include "arduino.h"
#include "Wire.h"
#include "EEPROM.h"

#include "CommonObjects.h"

namespace Cupteano {

    typedef struct {
        float x;
        float z;
        float y;
    } compassAxis;

    class HMC5883L
    {
    private:
        static constexpr int i2cAddress = 0x1E;

        float xMax, xMin;
        float yMax, yMin;
        float xOld, yOld;
        int16_t xFix, yFix;

        int firstAngle;

        bool isValueCorrect = true;

        TwoWire * i2c;

    public:
        compassAxis getRawAxis();
        int getRawDegree();
        
        HMC5883L(TwoWire&);
        int getAngle();
        void useOldCalibData();
        void beginCalibration();
        void refreshCalibration();
        void endCalibration(); 
        operator int();
    };

}

#endif
