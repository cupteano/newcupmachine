
#include "Kicker.h"

using namespace Cupteano;

Kicker::Kicker(uint8_t _pin, uint8_t _chargePin) : pin(_pin), chargePin(_chargePin)
{
    pinMode(pin,       OUTPUT);
    pinMode(chargePin, OUTPUT);
}

void Kicker::kick()
{
    digitalWrite(chargePin, HIGH);
    
    digitalWrite(pin, HIGH);
    delay(waitTime);
    digitalWrite(pin, LOW);

    digitalWrite(chargePin, LOW);

    delay(timeUntilKickerReturn);

    chargeStartedTime = 0;
}

void Kicker::chargeEventRun()
{
    if (! chargeStartedTime) {
        
        // start charging
        digitalWrite(chargePin, HIGH);
        chargeStartedTime = millis();
        
    } else if (millis() - chargeStartedTime >= fullChargeTime) {

        // end chargeing
        digitalWrite(chargePin, LOW);
        
    }
}

