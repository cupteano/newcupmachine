
#include "HoldSensor.h"

using namespace Cupteano;

HoldSensor::HoldSensor(uint8_t _pin) : pin(_pin)
{
    pinMode(pin, INPUT);
}

bool HoldSensor::read()
{
    return analogRead(pin) < threshold;
}

HoldSensor::operator bool()
{
    return this->read();
}

