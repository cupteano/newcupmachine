
#ifndef IRSerial_h
#define IRSerial_h

#include "arduino.h"

namespace Cupteano {

    class IRSerial {
        
    private:
        HardwareSerial * serial;

    union {
        uint16_t data;
            struct {
                uint8_t lower;
                uint8_t upper;
            };
    } data16;

        
    public:
        IRSerial(HardwareSerial*);
        int getAngle();
        int getDistance();
    
    };

}

#endif

