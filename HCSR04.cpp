
#include "HCSR04.h"
using namespace Cupteano;

HCSR04::HCSR04(uint8_t _trigPin, uint8_t _echoPin) : trigPin(_trigPin), echoPin(_echoPin)
{
    pinMode(trigPin, OUTPUT);
    pinMode(echoPin, INPUT);
}

int HCSR04::getRawValue()
{
    digitalWrite(trigPin,HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin,LOW);
    return pulseIn(echoPin, HIGH, 20000);
}

float HCSR04::getDistanceAsCentimeter()
{
    return ((float)this->getRawValue() / 2.0) * sonicSpeed * 100 / 1000000; // (t / 2) ms * 340 m/s * (100 cm/m) / (1000000 us/s)
}

HCSR04::operator float()
{
    return this->getDistanceAsCentimeter();
}

