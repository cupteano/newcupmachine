
#include "AppDelegate.h"

#define ENABLE_BALLTRACK
#define ENABLE_KICKER
#define ENABLE_LINE

//#define TEST_OMNI
//#define TEST_PID

//#define PRINT_COMPASS
//#define PRINT_IR
//#define PRINT_LINE
//#define PRINT_HOLD

AppDelegate::AppDelegate()
{
        
}

void AppDelegate::begin()
{

    tone(11, 4500);
    delay(1500);
    noTone(11);
  
    for (int i = 0; i < 3; i++) {
    delay(500);
    tone(11, 4000);
    delay(40);
    noTone(11);
    }

    if (toggleSwitch) {
        calibration();
    } else {
        compass.useOldCalibData();
    }

    Cupteano::MotorDriver::enable();
    
    while (true) {
        
        if (toggleSwitch) {
            this->mainLoop();
        } else {
            motorLeft = 0;
            motorBack = 0;
            motorRight = 0;
        }

        if (serialEventRun) serialEventRun();
        
    } // while (ture)
}

void AppDelegate::mainLoop()
{

#ifdef ENABLE_BALLTRACK
    ballTrack();
#endif
#ifdef ENABLE_KICKER
    kicker.chargeEventRun();
    if (holdSensor) {
      tone(11, 5000, 300);
      kicker.kick();
    }
#endif
#ifdef ENABLE_LINE
    if (lineLeft || lineRight || lineFront || lineBack) 
    tone(11, 5000, 200);

    unsigned long currentTime = millis();

    if (lineLeft) {
        tone(11, 5000, 20);
        while (millis() - currentTime <= 50) run(90, 240);
        motorLeft = 0;
        motorBack = 0;
        motorRight = 0;
    } else if (lineRight) {
        tone(11, 5000, 20);
        while (millis() - currentTime <= 50) run(-90, 240);
        motorLeft = 0;
        motorBack = 0;
        motorRight = 0;
    } else if (lineFront) {
        tone(11, 5000, 20);
        while (millis() - currentTime <= 50) run(180, 240);
        motorLeft = 0;
        motorBack = 0;
        motorRight = 0;
    } else if (lineBack) {
        tone(11, 5000, 20);
        while (millis() - currentTime <= 50) run(0, 240);
        motorLeft = 0;
        motorBack = 0;
        motorRight = 0;
    }
#endif
#ifdef TEST_OMNI
    run(90, 200);
#endif
#ifdef TEST_PID
    turn(PIDController(compass));
#endif

#ifdef PRINT_COMPASS
    Serial.println(compass);
#endif
#ifdef PRINT_IR
    Serial.print(ballSensor.getAngle());
    Serial.print(",");
    Serial.println(ballSensor.getDistance());
#endif
#ifdef PRINT_LINE
    Serial.print(lineLeft);
    Serial.print("\t");
    Serial.print(lineRight);
    Serial.print("\t");
    Serial.print(lineFront);
    Serial.print("\t");
    Serial.print(lineBack);
    Serial.print("\t");
    Serial.println();

    Serial.print(lineLeft.getRawValue());
    Serial.print("\t");
    Serial.print(lineRight.getRawValue());
    Serial.print("\t");
    Serial.print(lineFront.getRawValue());
    Serial.print("\t");
    Serial.print(lineBack.getRawValue());
    Serial.print("\t");
    Serial.println();
#endif
#ifdef PRINT_HOLD
    Serial.println(analogRead(A9));
#endif

}

void AppDelegate::calibration()
{
    compass.beginCalibration();
    while (toggleSwitch) compass.refreshCalibration();
    compass.endCalibration();
}

void AppDelegate::move(int degree, int speed)
{
    
    int recAngle = Cupteano::robotToRecCoordinate(degree);

    float xVector = cos(radians(recAngle)) * speed;
    float yVector = sin(radians(recAngle)) * speed;

//    if (sonicLeft  < 30)  if (xVector < 0) xVector = 0;
//    if (sonicRight < 30)  if (xVector > 0) xVector = 0;
//
//    if (sonicBack  < 30)  if (yVector < 0) yVector = 0;
//    if (sonicBack  > 200) if (yVector > 0) yVector = 0;

    float resultAngle = degrees(atan2(yVector, xVector));
    float resultSpeed = sqrt(xVector * xVector + yVector * yVector);

    float fix = PIDController(compass);
//    float fix = 0;

    Serial.println((int)resultAngle);

    motorLeft  = sin(radians(resultAngle - 150)) * speed + fix;
    motorBack  = sin(radians(resultAngle - 270)) * speed + fix;
    motorRight = sin(radians(resultAngle - 30))  * speed + fix;
    
}

void AppDelegate::run(int degree, int speed){
    float fix = PIDController(compass);
//    float fix = 0;

    motorLeft  = sin(radians(degree - 480)) * speed + fix;
    motorBack  = sin(radians(degree - 360)) * speed + fix;
    motorRight = sin(radians(degree - 240)) * speed + fix;
}

int AppDelegate::turn(int speed)
{
    motorLeft = speed;
    motorBack = speed;
    motorRight = speed;
}


int AppDelegate::ballTrack() {

    int ball_distance = ballAngleSmoother.smooth(ballSensor.getDistance());
    int ball_angle = Cupteano::recToRobotCoordinate(ballSensor.getAngle());

    if (ball_distance == 0) {
        //if cannot see the ball
        motorLeft = 0;
        motorBack = 0;
        motorRight = 0;
        return 0;
    }

    ball_distance = constrain(map(ballDistanceSmoother.smooth(ball_distance), 40, 200, 30, 80), 30, 80);

    if (abs(ball_angle) < 20) {
        //if the ball is in front
        run(0, 200);
//        tone(11, 5000, 20);
    } else {

        if (ball_angle > 0) {
            // wraparound anti-clockwise
            run(ball_angle + (ball_distance), 200);
        } else {
            // wraparound clockwise
            run(ball_angle - (ball_distance), 200);
        }

    }
}

int AppDelegate::PIDController(int deflection)
{
    constexpr float Ku = 1.3;
    constexpr float Pu = 3.0;

    constexpr float Kp = Ku * 0.6;
    constexpr float Ki = Pu * 0.5;
    constexpr float Kd = Pu * 0.83;
    
    constexpr int integralTimes = 30;

    deflection = compassSmoother.smooth(deflection);

    static int deflectionBuffer[integralTimes] = {};
    static unsigned long oldTime = millis();
    static float integralOfDeflection = 0;

    for (int i = 0; i < integralTimes - 1; i++) deflectionBuffer[i] = deflectionBuffer[i + 1];
    
    deflectionBuffer[integralTimes - 1] = deflection;
    integralOfDeflection = integralOfDeflection - deflectionBuffer[0] * ((millis() - oldTime) * 0.001f) + deflection * ((millis() - oldTime) * 0.001f);

    float diffrentialOfDeflection = constrain((deflection - deflectionBuffer[integralTimes - 2])/((millis() - oldTime) * 0.001f), -20, 20);


    float operation = Kp * deflection + Ki * integralOfDeflection + Kd * diffrentialOfDeflection;

    oldTime = millis();
    
    return (int)operation;
}

