
#ifndef CommonObjects_h
#define CommonObjects_h

namespace Cupteano {
    
    constexpr int safeDegrees(int value, int min, int max);
    int recToRobotCoordinate(int recDegree);
    int robotToRecCoordinate(int robotDegree);
    
}

#endif
