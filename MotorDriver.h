
#ifndef MotorDriver_h
#define MotorDriver_h

#include "arduino.h"

namespace Cupteano {

    class MotorDriver {

    private:
        const uint8_t pinA;
        const uint8_t pinB;
        const uint8_t pinP;

        static bool motorEnabled;

    public:
        MotorDriver(uint8_t _pinA, uint8_t _pinB, uint8_t _pinP);
        void spin(int speed);
        void brake();
        void coast();
        static void enable();
        static void disable();
        MotorDriver& operator= (int value);
        ~MotorDriver();
        
    };

}

#endif
