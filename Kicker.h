
#ifndef Kicker_h
#define Kicker_h

#include "arduino.h"

namespace Cupteano {

    class Kicker {

    private:
        static constexpr unsigned long waitTime              = 20;
        static constexpr unsigned long timeUntilKickerReturn = 100;
        static constexpr unsigned long fullChargeTime        = 5000;
        
        const uint8_t pin;
        const uint8_t chargePin;

        unsigned long chargeStartedTime = 0;

    public:
        Kicker(uint8_t _pin, uint8_t _chargePin);
        void kick();
        void chargeEventRun();

    };
}

#endif
