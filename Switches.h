
#ifndef Switches_h
#define Switches_h

#include "arduino.h"

namespace Cupteano {

    class Switches {
    
    private:
        uint8_t pin;
    
    public:
        Switches(uint8_t _pin);
        bool read();
        operator bool();
    
    };
  
}

#endif
