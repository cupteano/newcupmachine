
#ifndef HoldSensor_h
#define HoldSensor_h

#include "arduino.h"

namespace Cupteano {

    class HoldSensor {

    private:
        const uint8_t pin;
        static constexpr int threshold = 500;

    public:
        HoldSensor(uint8_t _pin);
        bool read();
        operator bool();
        
    };
}

#endif
