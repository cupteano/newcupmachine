
#include "HMC5883L.h"

using namespace Cupteano;

HMC5883L::HMC5883L(TwoWire &_i2c) : i2c(&_i2c)
{
    i2c->begin();
    i2c->beginTransmission(i2cAddress);
    i2c->write(2);
    i2c->write(0);
    i2c->endTransmission();
}


int HMC5883L::getAngle()
{
    int degree = getRawDegree() - firstAngle;
    if(degree > 180){
      degree -= 360;
    }else if (degree <= -180){
      degree += 360;
    }
    return isValueCorrect ? degree : 0;
}

void HMC5883L::useOldCalibData()
{
    xFix = (EEPROM.read(0) << 8) | (EEPROM.read(1));
    yFix = (EEPROM.read(2) << 8) | (EEPROM.read(3));
    firstAngle = (EEPROM.read(4) << 8) | (EEPROM.read(5));
}

void HMC5883L::beginCalibration()
{
    xFix = yFix = 0;
    firstAngle = 0;

    compassAxis axis = getRawAxis();
    xMax = xMin = xOld = axis.x;
    yMax = yMin = yOld = axis.y;
}

void HMC5883L::refreshCalibration()
{
    compassAxis axis = getRawAxis();
    
    if(axis.x > xMax) xMax = axis.x;
    else if(axis.x < xMin) xMin = axis.x;
    if(axis.y > yMax) yMax = axis.y;
    else if(axis.y < yMin) yMin = axis.y;
}

void HMC5883L::endCalibration()
{
    xFix = (xMax + xMin) / 2;
    yFix = (yMax + yMin) / 2;
  
    firstAngle = getAngle(); 

    uint8_t xFixUpperBits = xFix >> 8;
    uint8_t xFixLowerBits = xFix & 0x00ff;

    uint8_t yFixUpperBits = yFix >> 8;
    uint8_t yFixLowerBits = yFix & 0x00ff;

    uint8_t firstAngleUpperBits = firstAngle >> 8;
    uint8_t firstAngleLowerBits = firstAngle & 0x00ff;

    EEPROM.write(0, xFixUpperBits);
    EEPROM.write(1, xFixLowerBits);
    EEPROM.write(2, yFixUpperBits);
    EEPROM.write(3, yFixLowerBits);
    EEPROM.write(4, firstAngleUpperBits);
    EEPROM.write(5, firstAngleLowerBits);
}

compassAxis HMC5883L::getRawAxis()
{
    compassAxis rawAxis;
    unsigned long requestStartTime = millis();
  
    i2c->beginTransmission(i2cAddress);
    i2c->write(3);
    i2c->endTransmission(i2cAddress);
  
    i2c->requestFrom(i2cAddress,6);
    while (i2c->available() < 6) {
        if (millis() - requestStartTime > 70) {
            while (i2c->available()) i2c->read();
            rawAxis.x = 0;
            rawAxis.z = 0;
            rawAxis.y = 0;
            
            isValueCorrect = false;

            return rawAxis;
        }
    }

    rawAxis.x = (i2c->read() << 8 | i2c->read());
    rawAxis.z = (i2c->read() << 8 | i2c->read());
    rawAxis.y = (i2c->read() << 8 | i2c->read());

    isValueCorrect = true;

    return rawAxis;
}

int HMC5883L::getRawDegree()
{
    compassAxis axis = getRawAxis();
    return (int)degrees(atan2(axis.y - yFix, axis.x - xFix));
}

HMC5883L::operator int()
{
    return getAngle();
}


