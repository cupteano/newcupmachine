#define DEBUG

#include "AppDelegate.h"

void setup()
{
#   ifdef DEBUG
        Serial.begin(9600);
#   endif
}

void loop()
{
    AppDelegate delegate;
    delegate.begin();
}
