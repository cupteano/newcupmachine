
#ifndef LineSensor_h
#define LineSensor_h

#include "arduino.h"

#include "MovingAverage.h"

namespace Cupteano {

    class LineSensor {

    private:
        MovingAverage lineSmoother = MovingAverage(10);
    
        const int lineSensorThreshold;
        const uint8_t pin;

    public:
        LineSensor(uint8_t _pin, int _lineSensorThreshold);
        bool read();
        int getRawValue();
        operator bool();
        
    };
}

#endif
