
#include "IRSerial.h"

#include "CommonObjects.h"

using namespace Cupteano;

IRSerial::IRSerial(HardwareSerial *_serial) : serial(_serial)
{
    serial->begin(9600);
}


int IRSerial::getAngle()
{
    serial->write('A');
    while(! (serial->available() == 2));
    int result = serial->read() << 8 | serial->read();
    return result;
}

int IRSerial::getDistance()
{
    serial->write('B');
    while(! (serial->available() == 2));
    int result = serial->read() << 8 | serial->read();
    return result;
}




