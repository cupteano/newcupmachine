
#include "Switches.h"

using namespace Cupteano;

Switches::Switches(uint8_t _pin) : pin(_pin)
{
    pinMode(pin, INPUT);
}

bool Switches::read()
{
    return digitalRead(pin) * -1 + 1;
}

Switches::operator bool()
{
    return read();
}

