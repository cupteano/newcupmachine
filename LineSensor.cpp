
#include "LineSensor.h"

using namespace Cupteano;

LineSensor::LineSensor(uint8_t _pin, int _lineSensorThreshold) : pin(_pin), lineSensorThreshold(_lineSensorThreshold)
{
    pinMode(pin, INPUT);
}

bool LineSensor::read()
{
    return this->getRawValue() > lineSensorThreshold;
}

int LineSensor::getRawValue()
{
    return lineSmoother.smooth(analogRead(pin));
}

LineSensor::operator bool()
{
    return this->read();
}

