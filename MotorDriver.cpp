
#include "MotorDriver.h"

using namespace Cupteano;

bool MotorDriver::motorEnabled;

MotorDriver::MotorDriver(uint8_t _pinA, uint8_t _pinB, uint8_t _pinP) : pinA(_pinA), pinB(_pinB), pinP(_pinP)
{
    motorEnabled = false;
    
    pinMode(pinA, OUTPUT);
    pinMode(pinB, OUTPUT);
    pinMode(pinP, OUTPUT);
}

void MotorDriver::spin(int speed)
{
    if (! motorEnabled) {
        coast();
        return;
    }
    
    if (speed > 0) {
        digitalWrite(pinA, HIGH);
        digitalWrite(pinB, LOW);
    } else if (speed < 0) {
        digitalWrite(pinA, LOW);
        digitalWrite(pinB, HIGH);
    } else {
        digitalWrite(pinA, LOW);
        digitalWrite(pinB, LOW);
    }

    analogWrite(pinP, abs(speed));
}

void MotorDriver::coast()
{
    this->spin(0);
}

void MotorDriver::brake()
{
    digitalWrite(pinA, HIGH);
    digitalWrite(pinB, HIGH);
    analogWrite(pinP, 0);
}

void MotorDriver::enable()
{
    motorEnabled = true;
}

void MotorDriver::disable()
{
    motorEnabled = false;
}

MotorDriver& MotorDriver::operator= (int value)
{
    this->spin(value);
    return *this;
}

MotorDriver::~MotorDriver()
{
    this->coast();
}

